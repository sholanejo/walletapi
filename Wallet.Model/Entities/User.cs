﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Wallet.Model.Entities
{
    public class User : IdentityUser
    {
        public User()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsDeleted { get; set; }

    }
}
