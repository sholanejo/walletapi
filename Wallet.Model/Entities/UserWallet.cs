﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Wallet.Model.Entities
{
    public class UserWallet
    {
        public Guid Id { get; set; }

        public Guid WalletId { get; set; }

        [ForeignKey(nameof(User))]
        public string UserId { get; set; }

        [Column(TypeName = "decimal(38,2)")]
        public decimal Balance { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public bool IsDeleted { get; set; }

        public virtual User User { get; set; }

    }
}
