﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Wallet.Model.Entities
{
    public class Transaction
    {
        public Transaction()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }

        public Guid Id { get; set; }

        [ForeignKey("Wallet")]
        public Guid WalletId { get; set; }

        public decimal Amount { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public bool IsDeleted { get; set; }

    }
}
